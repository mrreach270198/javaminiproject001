package Controllers;

import Event.EventClass;
import Menu.Menu;
import View.Header;
import Event.EventClass;
import java.io.IOException;


public class Main{

    public Main(){
        EventClass.loadData();
    }

    public static void main(String[] args) throws IOException{
        new Main();
        Header.header();
        EventClass.checkMissSave();
        Menu.menu();
    }
}
