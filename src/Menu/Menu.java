package Menu;

import Event.EventClass;
import View.Header;
import java.io.IOException;
import java.util.Scanner;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
public class Menu {
    private static String proName;
    private static double unitPrice;
    private static int qty;
    private static int id;
    private final static Scanner sc = new Scanner(System.in);
    public static void menu() throws IOException {
        do {
            Header.category();
            String command = select();
            switch (command) {
                case "*","F","f" -> {
                    EventClass.display(1);
                }
                case "W", "w" -> {
                    EventClass.write();
                }
                case "R", "r" -> {
                    EventClass.readData();
                }
                case "U", "u" -> {
                    EventClass.updatePro();
                }
                case "D", "d" -> {
                    EventClass.deletePro();
                }
                case "P", "p" -> {
                    System.out.println("This is Previous page");
                    int n;
                    if(EventClass.getTabnumber() > 1){
                        n = EventClass.getTabnumber() - 1;
                    }else {
                        n = 1;
                    }
                    EventClass.display(n);
                }
                case "N", "n" -> {
                    System.out.println("This is Next page");
                    int n = 1;
                    if(EventClass.getTabnumber() < EventClass.getMaxpage() ){
                        n = EventClass.getTabnumber() + 1;
                    }else {
                        n = EventClass.getTabnumber();
                    }
                    EventClass.display(n);
                }
                case "L", "l" -> {
                    System.out.println("This is Last page");
                    EventClass.display(EventClass.getMaxpage());
                }
                case "S", "s" -> {
                    EventClass.searchPro();
                }
                case "G", "g" -> {
                    System.out.print("Please choose page number from 1 to "+EventClass.getMaxpage()+" : ");
                    if (EventClass.checkInput()) {
                        int choosePage = Integer.parseInt(EventClass.input);
                        if (choosePage > EventClass.getMaxpage()) {
                            EventClass.display(EventClass.getMaxpage());
                        } else if (choosePage < 1) {
                            EventClass.display(1);
                        } else {
                            EventClass.display(choosePage);
                        }
                    }
                }
                case "Se", "se" -> {
                    System.out.print("Input Row: ");
                    if (EventClass.checkInput()) {
                        int inputRow = Integer.parseInt(EventClass.input);
                        EventClass.setRow(inputRow);
                        EventClass.display(1);
                    }
                }
                case "Sa", "sa" -> {
                    EventClass.save();
                }
                case "Ba", "ba" -> {
                    System.out.print("Do you want to back up all the record? [Y/y] or [N/n] --> ");
                    String op = sc.next();
                    if (op.equalsIgnoreCase("Y")){
                        EventClass.backUp();
                    }
                }
                case "Re", "re" -> {
                    EventClass.restore();
                }
                case "H", "h" -> {
                    help();
                }
                case "W#","w#" -> {
                    EventClass.writeData(proName,unitPrice,qty);
                }
                case "R#" ,"r#"->{
                    EventClass.read(id);
                }
                case "D#" , "d#" ->{
                    EventClass.delete(id);
                }
                case "E", "e" -> {
                    EventClass.exit();
                }
                case "10m" -> EventClass.millionRecord();
                case "reid" -> EventClass.resetId();
                default -> System.out.println("Your command is invalid!");
            }
        }while (true);
    }

    //    Cho0se siwtch
    public static String select(){
        System.out.print("Command --> ");
        String command = sc.next();
        String[] select = command.split("[#/-]");
        if(select.length == 4){
            command = select[0]+"#";
            proName = select[1];
            unitPrice = Double.parseDouble(select[2]);
            qty = Integer.parseInt(select[3]);
        }else if(select.length == 2){
            command = select[0] + "#";
            id = Integer.parseInt(select[1]);
        }
        return command;
    }

    //    Help
    public static void help(){
        CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
        Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
        t.addCell("1.   press    *   : Display all record of products",cellStyle);
        t.addCell("2.   press    W   : Adde new product",cellStyle);
        t.addCell("     press    w#proname,UnitPrice,qty: shortcut for add new product",cellStyle);
        t.addCell("3.   press    r   : Read Content of any conten",cellStyle);
        t.addCell("     press    r#id : Shortcut for read product by ID",cellStyle);
        t.addCell("4.   press    u   : Update product",cellStyle);
        t.addCell("5.   press    d   : Delete product by id",cellStyle);
        t.addCell("     press    d#id  : Shortcut delete product by ID",cellStyle);
        t.addCell("6.   press    f   : For display first page",cellStyle);
        t.addCell("7.   press    p   : For display previous page",cellStyle);
        t.addCell("8.   press    n   : For display next page",cellStyle);
        t.addCell("9.   press    l   : For display last page",cellStyle);
        t.addCell("10.  press    s   : Search product by name",cellStyle);
        t.addCell("11.  press    sa  : save record file",cellStyle);
        t.addCell("12.  press    ba  : Backup data",cellStyle);
        t.addCell("13.  press    re  : Restore data",cellStyle);
        t.addCell("14.  press    h   : Help",cellStyle);
        t.addCell("15.  press    10m : 10 million record",cellStyle);
        t.addCell("16.  press    reid : Sort ID",cellStyle);
        System.out.println(t.render());
    }
}
