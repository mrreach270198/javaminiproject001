package Event;

import Model.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static java.lang.System.in;

public class EventClass {
    static Scanner sc = new Scanner(in);
    static ArrayList<Product> al = new ArrayList<>();
    static ArrayList<Product> temp =new ArrayList<>();
    static int id;
    public static String input;
    public static long durationInMs;
    static String fileName = "src/File/Stock.txt";
    private static int tabnumber;
    private static int maxrecord;
    private static int row = 3;

public static void loadData(){
    long startTime = System.nanoTime();
    try{
        FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            String data;
            while ((data = br.readLine()) != null) {
                String[] ss = data.split(",");
                try {
                    al.add(new Product(Integer.parseInt(ss[0]), ss[1], Double.parseDouble(ss[2]), Integer.parseInt(ss[3]), ss[4]));
                }catch (NumberFormatException e){
                }
            }
            if (al != null){
                id = al.size()+1;
            }else {
                id = 1;
            }
            fr.close();
            br.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        durationInMs = TimeUnit.MILLISECONDS.convert(timeElapsed, TimeUnit.NANOSECONDS);
    }

    public static void checkMissSave() throws IOException {
        FileReader fr = new FileReader("src/File/temp.txt");
        BufferedReader br = new BufferedReader(fr);
        try {
            if (br.readLine() != null){
                System.out.print("You have missed save a record! Do you want to save it? [Y/y] or [N/n] : ");
                String ch = sc.next();
                if (ch.equalsIgnoreCase("Y")){
                    writeFromTemp();
                    clearTempFile();
                }else {
                    clearTempFile();
                    print("Save canceled!");
                }
            }
            fr.close();
            br.close();
        }catch (Exception e){

        }
    }

    public static void display ( int current){
        int i = row * current;
        int n = i - row;
        int page = 0;
        CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.ALL);
        t.addCell("    ID    ", cellStyle);
        t.addCell("     Name     ", cellStyle);
        t.addCell("  Unit Price  ", cellStyle);
        t.addCell("      Qty     ", cellStyle);
        t.addCell(" Imported Date ", cellStyle);
        for (Product product : al) {
            if (al.indexOf(product) >= n && al.indexOf(product) < i) {
                t.addCell(product.getId() + "", cellStyle);
                t.addCell(product.getName() + "", cellStyle);
                t.addCell(product.getUnitPrice() + "", cellStyle);
                t.addCell(product.getQty() + "", cellStyle);
                t.addCell(product.getImpDate() + "", cellStyle);
            } else if (al.indexOf(product) == i) {
                break;
            }
        }
        if (al.size() % row == 0) {
            page = al.size() / row;
        } else {
            page = (al.size() / row) + 1;
        }
        System.out.println(t.render());
        CellStyle cellStyle1 = new CellStyle(CellStyle.HorizontalAlign.left);
        Table t1 = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
        t1.addCell("page "+ current + " of " + page+"           -->>>>> Stock Management <<<<<--          All Record : "+ al.size(),cellStyle1);
        System.out.println(t1.render());
        maxrecord = page;
        tabnumber = current;
    }
    public static int getTabnumber () {
        return tabnumber;
    }
    public static int getMaxpage () {
        return maxrecord;
    }

    public static void setRow ( int rows){
        row = rows;
    }

    public static void write() {
        System.out.println("Input ID : " + id);
        System.out.print("Input Name : ");
        String name = sc.next();
        System.out.print("Input Unit price : ");
        if (checkInput()){
            String price = input;
            System.out.print("Input Qty : ");
            if (checkInput()){
                String qty = input;
                writeData(name, Double.parseDouble(price),Integer.parseInt(qty));
            }
        }
    }

    public static void writeData(String name,double price, int qty){
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
        Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
        t.addCell("ID = " + id, cellStyle);
        t.addCell("Name = " + name,cellStyle);
        t.addCell("Unit Price = " + price,cellStyle);
        t.addCell("Quantity = "+qty,cellStyle);
        t.addCell("Import Date = "+formatter.format(date),cellStyle);
        System.out.println(t.render());
        System.out.print("Do you want to add this record? [Y/y] or [N/n] --> ");
        String ch = sc.next();
        if (ch.equalsIgnoreCase("Y")) {
            recovery(id, name, price, qty, formatter.format(date));
            al.add((new Product(id,name,price,qty,formatter.format(date))));
            print("successfully added!");
            id++;
        }else {
            print("Canceled!");
        }
    }

    public static void writeFromTemp(){
        try{
            FileReader fr = new FileReader("src/File/temp.txt");
            BufferedReader br = new BufferedReader(fr);
            String data;
            while ((data = br.readLine()) != null) {
                String[] ss = data.split(",");
                try {
                    temp.add(new Product(Integer.parseInt(ss[0]), ss[1], Double.parseDouble(ss[2]), Integer.parseInt(ss[3]), ss[4]));
                }catch (NumberFormatException e){

                }
            }
            al.addAll(temp);
            save();
            fr.close();
            br.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void clearTempFile () {
        try {
            FileWriter writer = new FileWriter("src/File/temp.txt");
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write("");
            bwr.close();
        } catch (Exception e) {

        }
    }

    public static void millionRecord(){
        long startTime = System.nanoTime();
        try{
            FileWriter writer = new FileWriter("src/File/10M.txt");
            BufferedWriter bwr = new BufferedWriter(writer);

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            System.out.print("Are you sure that you want to regenerate 10 million record? [Y/y] or [N/n] --> ");
            String ch = sc.next();
            if (ch.equalsIgnoreCase("Y")) {
                for(int i = 0 ; i< 100000; i++){
                    int qty = (int) (Math.random()*(100+1));
                    double unitPrice = (Math.random()*(1000-10+1)+10);
                    DecimalFormat df = new DecimalFormat(".###");
                    unitPrice = Double.parseDouble(df.format(unitPrice));
                    bwr.write(i+1+","+generateName()+","+unitPrice+","+qty+","+formatter.format(date));
                    bwr.newLine();
                    bwr.flush();
                }
            }

            writer.close();
            bwr.close();

            long endTime = System.nanoTime();
            long timeElapsed = endTime - startTime;
            System.out.println("Time write : "+TimeUnit.MILLISECONDS.convert(timeElapsed, TimeUnit.NANOSECONDS));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static String generateName(){
        String[] name = {"Soda","Fanta","Zenya","Oichi","Aquarius","Black Cat", "Green Tea","Coffee"};
        return name[(int)(Math.random()*name.length)];
    }

    public static void readData(){
        System.out.print("Read by ID : ");
        if (checkInput()){
            int id = Integer.parseInt(input);
            read(id);
        }
    }
     public static void read(int id){

        for (Product product : al){
            if (id == product.getId()){
                CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
                Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
                t.addCell("ID = " + product.getId(), cellStyle);
                t.addCell("Name = " + product.getName(),cellStyle);
                t.addCell("Unit Price = " + product.getUnitPrice(),cellStyle);
                t.addCell("Quantity = "+product.getQty(),cellStyle);
                t.addCell("Import Date = "+product.getImpDate(),cellStyle);
                System.out.println(t.render());
            }
        }
    }

    public static void updatePro(){
        System.out.print("Input Product ID to Update : ");
        if (checkInput()) {
            int sid = Integer.parseInt(input);

            for (Product product : al) {
                if (sid == product.getId()) {
                    CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
                    Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
                    t.addCell("ID = " + product.getId(), cellStyle);
                    t.addCell("Name = " + product.getName(), cellStyle);
                    t.addCell("Unit Price = " + product.getUnitPrice(), cellStyle);
                    t.addCell("Quantity = " + product.getQty(), cellStyle);
                    t.addCell("Import Date = " + product.getImpDate(), cellStyle);
                    System.out.println(t.render());

                    System.out.print("Do you want to update this record? [Y/y] or [N/n] --> ");
                    String op = sc.next();
                    if (op.equalsIgnoreCase("Y")) {
                        Table t1 = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.ALL);
                        t1.addCell("1.All       " + "2.Name       " + "3.Quantity       " + "4.Unit Price        " + "5.Back to menu", cellStyle);
                        System.out.println(t1.render());
                        System.out.print("Which one do you want to update? --> ");
                        String ch = sc.next();
                        switch (ch) {
                            case "1" -> {
                                updateAll(product);
                            }
                            case "2" -> {
                                updateNamePro(product);
                            }
                            case "3" -> {
                                updateQty(product);
                            }
                            case "4" -> {
                                updatePrice(product);
                            }
                            case "5" -> {
                                return;
                            }
                            default -> System.out.println("Input invalid!");
                        }
                        Table t2 = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
                        t2.addCell("ID = " + product.getId(), cellStyle);
                        t2.addCell("Name = " + product.getName(), cellStyle);
                        t2.addCell("Unit Price = " + product.getUnitPrice(), cellStyle);
                        t2.addCell("Quantity = " + product.getQty(), cellStyle);
                        t2.addCell("Import Date = " + product.getImpDate(), cellStyle);
                        System.out.println(t2.render());
                        print("Update Successfully!");
                    } else {
                        print("Update canceled!");
                    }
                }
            }
        }
    }
    private static void updatePrice (Product product){
        System.out.print("Input new Unit Price : ");
        if (checkInput()){
            double price = Double.parseDouble(input);
            product.setUnitPrice(price);
        }
    }
    private static void updateQty (Product product){
        System.out.print("Input new Quantity : ");
        if (checkInput()){
            int qty = Integer.parseInt(input);
            product.setQty(qty);
        }
    }
    private static void updateNamePro (Product product){
        System.out.print("Input new Name : ");
        String name = sc.next();
        product.setName(name);
    }
    private static void updateAll (Product product){
        updateNamePro(product);
        updatePrice(product);
        updateQty(product);
    }

    public static void deletePro () {
        System.out.print("Input Product ID to delete : ");
        if (checkInput()){
            int id = Integer.parseInt(input);
            delete(id);
        }
    }
    public static void delete ( int id){
        for (int i = 0; i < al.size(); i++) {
            if (id == al.get(i).getId()) {
                CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
                Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
                t.addCell("ID = " + al.get(i).getId(), cellStyle);
                t.addCell("Name = " + al.get(i).getName(),cellStyle);
                t.addCell("Unit Price = " + al.get(i).getUnitPrice(),cellStyle);
                t.addCell("Quantity = "+al.get(i).getQty(),cellStyle);
                t.addCell("Import Date = "+al.get(i).getImpDate(),cellStyle);
                System.out.println(t.render());
                System.out.print("Do you want to delete this record? [Y/y] or [N/n] --> ");
                String op = sc.next();
                if (op.equalsIgnoreCase("Y")) {
                    al.remove(al.get(i));
                    print(" Delete successfully!");
                } else {
                    print("Delete canceled!");
                }
            }
        }
    }

    public static void searchPro(){

        System.out.print("Input Name Product to Search : ");
        String sName = sc.next();
        CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.ALL);
        t.addCell("ID", cellStyle);
        t.addCell("NAME", cellStyle);
        t.addCell("Unit Price", cellStyle);
        t.addCell("Quantity", cellStyle);
        t.addCell("Imported Date", cellStyle);
        for (Product product : al){
            if (product.getName() != null && product.getName().toLowerCase().contains(sName.toLowerCase())){
                t.addCell(product.getId() + "", cellStyle);
                t.addCell(product.getName() + "", cellStyle);
                t.addCell(product.getUnitPrice() + "", cellStyle);
                t.addCell(product.getQty() + "", cellStyle);
                t.addCell(product.getImpDate() + "", cellStyle);
            }
        }
        System.out.println(t.render());

    }

    public static void save() throws IOException {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter bwr = new BufferedWriter(writer);
        for (Product product : al) {
            bwr.write(product.toString());
        }
        bwr.close();
        clearTempFile();
        print("Save successfully!");
    }

    public static void backUp() throws IOException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
        Date date = new Date();
        FileWriter writer = new FileWriter("src/File/"+formatter.format(date)+".txt");
        BufferedWriter bwr = new BufferedWriter(writer);
        for (Product product : al) {
            bwr.write(product.toString());
        }
        bwr.close();
        print("Back up successfully in file "+formatter.format(date)+".txt" );
    }

    public static void restore(){
        long startTime = System.nanoTime();
        List<String> results = new ArrayList<>();
        File[] files = new File("src/File").listFiles();

        if(files != null)
        for (File file : files) {
            if (file.isFile()) {
                if (file.getName().equals("temp.txt")){
                    continue;
                }
                results.add(file.getName());
            }
        }
        System.out.println("***** Please choose file back up ******");
        int i = 1;
        for (String s : results){
            System.out.println(i+") "+s);
            i++;
        }
        System.out.print("Please choose file now : ");
        if (checkInput()) {
            int ch = Integer.parseInt(input);

            al.clear();
            fileName = "src/File/" + results.get(ch - 1);
        }
        try{
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            String data;
            while ((data = br.readLine()) != null) {
                String[] ss = data.split(",");
                try {
                    al.add(new Product(Integer.parseInt(ss[0]), ss[1], Double.parseDouble(ss[2]), Integer.parseInt(ss[3]), ss[4]));
                }catch (NumberFormatException e){

                }
            }
            fr.close();
            br.close();
            print("Restore successfully!");
        }catch (Exception e){
            e.printStackTrace();
        }
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("Time read : "+TimeUnit.MILLISECONDS.convert(timeElapsed, TimeUnit.NANOSECONDS));
    }

    public static void recovery(int id, String name, double price, int qty, String date){
        try{
            FileWriter writer = new FileWriter("src/File/temp.txt",true);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write(id+","+name+","+price+","+qty+","+date);
            bwr.newLine();
            bwr.close();
        }catch (Exception e){

        }
    }

    public static void exit() throws IOException {
        FileReader fr = new FileReader("src/File/temp.txt");
        BufferedReader br = new BufferedReader(fr);
        try {
            if (br.readLine() != null){
                System.out.print("You have missed save a record! Do you want to save it? [Y/y] or [N/n] : ");
                String ch = sc.next();
                if (ch.equalsIgnoreCase("Y")){
                    save();
                    clearTempFile();
                    System.exit(0);
                }else {
                    clearTempFile();
                    print("Save canceled!");
                    System.exit(0);
                }
            }else {
                System.exit(0);
            }
            fr.close();
            br.close();
        }catch (Exception e){

        }
    }

    public static void resetId() {
        for (int i=0; i<al.size(); i++){
            al.get(i).setId(i+1);
        }
        id = al.size()+1;
        print("ID reset successfully!");
    }

    public static void print(String s){
        CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
        Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);

        t.addCell("    -->>>>> "+s+" <<<<<--    ", cellStyle);
        System.out.println(t.render());
    }

    public static boolean checkInput(){
    boolean check;
        input = sc.next();
        Pattern pattern = Pattern.compile("^-?\\d+(\\.\\d+)?$");
        Matcher matcher = pattern.matcher(input);
        check = matcher.matches();
        if (check){
            return true;
        }else {
            System.out.println("Input Invalid!");
            System.out.print("Please Input Only Number : ");
            return checkInput();
        }
    }
}