package View;

import Event.EventClass;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

public class Header {
    static public void delay(String s){
        try{
            for (int i=0; i<s.length(); i++){
                System.out.print(s.charAt(i));
                Thread.sleep(100);
            }
            System.out.println("Current time loading : "+ EventClass.durationInMs);
        }catch (Exception e){
            System.out.println(e);
        }
    }
    public static void header(){
        Runnable runnable = () -> {
            try {
                System.out.println("\t\t\t\t\t\t\t\t\t   Welcome to");
                Thread.sleep(300);
                System.out.println("\t\t\t\t\t\t\t\t\tStock Management");
                Thread.sleep(200);
                System.out.println("\t\t\t  _____  _                             _____           _        _____ _  _   ");
                Thread.sleep(200);
                System.out.println( "\t\t\t |  __ \\| |                           |  __ \\         | |      / ____| || |  ");
                Thread.sleep(200);
                System.out.println("\t\t\t | |__) | |__  _ __   ___  _ __ ___   | |__) |__ _ __ | |__   | |  __| || |_ ");
                Thread.sleep(200);
                System.out.println("\t\t\t |  ___/| '_ \\| '_ \\ / _ \\| '_ ` _ \\  |  ___/ _ \\ '_ \\| '_ \\  | | |_ |__   _|");
                Thread.sleep(200);
                System.out.println("\t\t\t | |    | | | | | | | (_) | | | | | | | |  |  __/ | | | | | | | |__| |  | |  ");
                Thread.sleep(200);
                System.out.println("\t\t\t |_|    |_| |_|_| |_|\\___/|_| |_| |_| |_|   \\___|_| |_|_| |_|  \\_____|  |_|\n");
                String s = "Please Wait Loading.....\n";
                delay(s);
            }catch (Exception e){
                e.printStackTrace();
            }
        };
        runnable.run();
    }
    public static void category(){
        CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(9, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.ALL);
        t.addCell("*)Display", cellStyle);
        t.addCell("W)rite", cellStyle);
        t.addCell("R)ead", cellStyle);
        t.addCell("U)pdate", cellStyle);
        t.addCell("D)elete", cellStyle);
        t.addCell("F)irst", cellStyle);
        t.addCell("P)revious", cellStyle);
        t.addCell("N)ext", cellStyle);
        t.addCell("L)ast", cellStyle);
        t.addCell("S)earch", cellStyle);
        t.addCell("G)oto", cellStyle);
        t.addCell("Se)t row", cellStyle);
        t.addCell("Sa)ve", cellStyle);
        t.addCell("Ba)ck up", cellStyle);
        t.addCell("Re)store", cellStyle);
        t.addCell("H)elp", cellStyle);
        t.addCell("E)xit", cellStyle);
        System.out.println(t.render());
    }
}
