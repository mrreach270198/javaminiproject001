package Model;

import java.io.Serializable;

public class Product implements Serializable {
    private int id;
    private String name;
    private double unitPrice;
    private int qty;
    private String impDate;

    public Product(int id, String name, double unitPrice, int qty, String impDate) {
        this.id = id;
        this.name = name;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.impDate = impDate;
    }

    @Override
    public String toString() {
        return id+","+name+","+unitPrice+","+qty+","+impDate+"\n";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getImpDate() {
        return impDate;
    }

    public void setImpDate(String impDate) {
        this.impDate = impDate;
    }
}
